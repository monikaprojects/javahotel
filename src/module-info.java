module hotelMitFX {
	requires javafx.graphics;
	requires javafx.fxml;
	requires javafx.controls;
	requires javafx.base;
	opens de.main;
	opens de.frontend;
	opens de.backend;
	opens de.middletier;
}