package de.middletier;

import java.util.function.Predicate;
import de.backend.Zimmer;
import de.backend.ZimmerBooking;
import de.backend.ZimmerType;
import de.backend.Zimmer_Nummer;
import de.backend.Zimmer_Status;


public class Auswahl_Criterien {

	public static final Predicate<ZimmerBooking> RESERVEDROOMS = room->room.getStatus().equals(Zimmer_Status.RESERVED);
	public static final Predicate<ZimmerBooking> OCCUPIEDROOMS = room->room.getStatus().equals(Zimmer_Status.BESETZT);		
			
	// function to select rooms of particular type
	public static Predicate<Zimmer>  erstellenSameRoomType(ZimmerType selectedType){
		return zimmer-> zimmer.getZimmerType()== selectedType;
	}

	
	// function to select all enteries in booking table corresponding to this room
	public static Predicate<ZimmerBooking>  erstellenSameRoomKriterium(Zimmer room){
		return zimmerbooking-> zimmerbooking.getZimmerNummer().equals(room.getZimmerNummer());
	}
	
	// function to select room in  zimmer table corresponding to this room
	public static Predicate<Zimmer>  erstellenSameRoomAsThis(Zimmer_Nummer roomnumber){
		return zimmer-> zimmer.getZimmerNummer().equals(roomnumber);
	}
	
}
