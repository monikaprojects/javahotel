package de.middletier;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import de.backend.Customer;
import de.backend.Mitarbeiter;
import de.backend.Person;
import de.backend.ZimmerDAO;
import de.backend.ZimmerType;
import de.backend.Zimmer_Nummer;
import de.backend.Zimmer_Status;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import de.backend.UserDAO;
import de.backend.Zimmer;
import de.backend.ZimmerBooking;
import de.backend.ZimmerBookingDAO;

// middletier to seperate direct link o user to database

public class Service_All {

	private List<Person> alleUser;
	private UserDAO datenquelleuser; 
	private List<Zimmer> allRooms;
	private ZimmerDAO datenquelleroom;
	private List<ZimmerBooking> allBookedRooms;
	private ZimmerBookingDAO datenquellebooking;
	
	
	/**
	 * @param alleUser
	 * @param datenquelleuser
	 */
	public Service_All(UserDAO datenquelle,ZimmerDAO datenquelleRooms,ZimmerBookingDAO datenquellebooking) {
		this.datenquelleuser = datenquelle;
		this.alleUser = datenquelleuser.getAllUsers();
		this.datenquelleroom = datenquelleRooms;
		this.allRooms = datenquelleroom.getAllRooms();
		this.datenquellebooking = datenquellebooking;
		this.allBookedRooms = datenquellebooking.getAllBooking();
		
	}
	

	
	//----------------------------------------methods
	
	//checks if this username and password is in user list or not. returns true in that case else false 
	// raise exception NotValidUser
	public boolean checkUserFromList(String user, String pwd) {
		
		
		for (int i=0;i<alleUser.size();i++) {
			if (alleUser.get(i).isEmployee()) {
				Mitarbeiter employee = (Mitarbeiter)alleUser.get(i);
				if ((employee.getUsername().equals(user))&&(employee.getPassword().equals(pwd))) {
					return true;
				}				
			}else {
				Customer customer = (Customer)alleUser.get(i);
				if ((customer.getUsername().equals(user))&&(customer.getPassword().equals(pwd))) {
					return true;
				}
			}	
		}
		// if user is not found in list and loop finishes
		try {
			throw new NotValidUser();
		}catch(NotValidUser ausnahme) {
			Alert alert=new Alert(AlertType.ERROR);
			alert.setContentText("There is no user by this name. Please enter valid User id and password");
			alert.showAndWait();
			//System.out.println("There is no user by this name. Please enter valid User id and password");
		}
		return false;
	}
	
	// using predicate interface method to select list of users fufilling a particular criteria
	public List<Person> auswaehlenUser(Predicate<Person>  bedingung){
		return alleUser.stream().filter(bedingung).collect(Collectors.toList());
	}
	
	// using predicate interface method to select list of rooms fufilling a particular criteria
	public List<Zimmer> auswaehlenRooms(Predicate<Zimmer>  bedingung){
		return allRooms.stream().filter(bedingung).collect(Collectors.toList());
	}

	//function to choose from booking list of rooms fufilling a particular criteria
	public List<ZimmerBooking> auswaehlBooking(Predicate<ZimmerBooking>  bedingung){
			return allBookedRooms.stream().filter(bedingung).collect(Collectors.toList());
	}
	
	
	// returns list of all rooms
	public List<Zimmer> getAllRooms(){
		return allRooms;	
	}
	

	// handles booking of rooms
	/*
	 * noOfPerson   -- No of  Adult People for whom room is needed
	 * noOfKids  	-- No of  kids People for whom room is needed
	 *  dateOfArrival -- Date of arrival of customer
	 *   dateOfLeaving-- Date of leaving of customer
	 *    roomsTypeAsked -- TYPE OF ROOMS DEMANDED by customer
	 *     clientName	-- name of customer
	*	 clientPhone 	-- phone no of customer
	*	  clientAddress	--address of customer
	*	   userName 	-- username who is doing the booking. can be customer or employee
 	*	roomsAllocatedStatus	-- return value for seeing which room types are available and which not
	 * 
	 * 
	 * returns boolean value if rooms are sufficient for these many people else false
	 */
	public boolean buchen(int noOfPerson,int noOfKids,LocalDate dateOfArrival,LocalDate dateOfLeaving,
											List<ZimmerType> roomsTypeAsked,String clientName,
											String clientPhone ,String clientAddress,String userName, List<Boolean> roomsAllocatedStatus) {
		
		Zimmer freeRoom = null;
		List<ZimmerType> roomsTypeAskedLocal =new ArrayList<ZimmerType>();
		List<Zimmer> listPossibleRooms = new ArrayList<Zimmer>();
		
		roomsTypeAskedLocal.addAll(roomsTypeAsked) ;
		
		for ( ZimmerType roomType : roomsTypeAsked) {
			if (checkIsRoomSufficient(noOfPerson, noOfKids,roomsTypeAskedLocal)) {
				listPossibleRooms = auswaehlenRooms(Auswahl_Criterien.erstellenSameRoomType(roomType));
				freeRoom = chkRoomAvailablílity(dateOfArrival,dateOfLeaving,listPossibleRooms);
				if (freeRoom != null){
					//   if date of arrival is for future then set status as reserved else besetzt
					if(dateOfArrival.isAfter(LocalDate.now())) {
						datenquellebooking.addBooking(new ZimmerBooking(freeRoom.getZimmerNummer(),Zimmer_Status.RESERVED,
							dateOfArrival,dateOfLeaving,getIdwithName(clientName,clientAddress,clientPhone),
							getIdwithuserName(userName)));
					}else {
						datenquellebooking.addBooking(new ZimmerBooking(freeRoom.getZimmerNummer(),Zimmer_Status.BESETZT,
								dateOfArrival,dateOfLeaving,getIdwithName(clientName,clientAddress,clientPhone),
								getIdwithuserName(userName)));
					}
					roomsAllocatedStatus.add(true);
				}else{
					roomsAllocatedStatus.add(false);
					roomsTypeAskedLocal.remove(roomType);
				}
			}
			else{					// if selected rooms are not sufficient for give no of people 
				return false;
			}
		}
		allBookedRooms.forEach(r-> System.out.println(r));
		alleUser.forEach(r-> System.out.println(r));
		return true;
	}

	

// checks from available room of a particular type if they are free on dates desired by customer
	/*
	 *  dateOfArrival -- Date of arrival of customer
	 *   dateOfLeaving-- Date of leaving of customer
	 * roomsPossible	-- checks from this list of rooms
	 * 
	 * returns room which can be allocated or which is free
	 * else null
	 */
	private Zimmer chkRoomAvailablílity(LocalDate dateOfArrival, LocalDate dateOfLeaving, List<Zimmer> roomsPossible) {
	
		Zimmer foundEntry = null;
		boolean roomAvailable = false;
		Zimmer room;
		int j=0;
		
		while ( !roomAvailable && (j<roomsPossible.size()) ) {
			roomAvailable = true;
			room=roomsPossible.get(j);
			
			List<ZimmerBooking> listParticularRoom= auswaehlBooking(Auswahl_Criterien.erstellenSameRoomKriterium(room));
			for ( ZimmerBooking booking : listParticularRoom) {
				if(booking.getStatus().equals(Zimmer_Status.RESERVED)) {
					if(booking.getStatusActiveOn().isBefore(dateOfArrival)&& booking.getStatusDeactiveOn().isAfter(dateOfArrival)) {
						roomAvailable = false;
					}
					if(booking.getStatusActiveOn().isBefore(dateOfLeaving)&& booking.getStatusDeactiveOn().isAfter(dateOfLeaving)) {
						roomAvailable = false;
					}
				}else if(booking.getStatus().equals(Zimmer_Status.NICHTGEEIGNET)) {
					if(booking.getStatusActiveOn().isBefore(dateOfArrival)&& booking.getStatusDeactiveOn().isAfter(dateOfArrival)) {
						roomAvailable = false;
					}
				}else if(booking.getStatus().equals(Zimmer_Status.BESETZT)) {
					roomAvailable = false;
				}else if(booking.getStatus().equals(Zimmer_Status.AUFRAUMEN)) {
					if(booking.getStatusActiveOn().isBefore(dateOfArrival)&& booking.getStatusDeactiveOn().isAfter(dateOfArrival)) {
						roomAvailable = false;
					}
				}
				if (roomAvailable) {
						// change booking table
						datenquellebooking.deleteBooking(booking);	
						foundEntry=room;
						break;
				}
			
			}	
			j++;
			
		}
		return foundEntry;
	}

	
	// function to check if selected rooms are sufficient for given no of people 
	/*noOfPerson		-- no of adults
	 * noOfKids			-- no of kids
	 * roomsAsked		-- list of room type asked by customer
	 * 
	 * returns true if this list is sufficient
	 * else false
	 */
	private boolean checkIsRoomSufficient(int noOfPerson, int noOfKids, List<ZimmerType> roomsAsked) {

		int valid=0;
		
		
		for ( ZimmerType room : roomsAsked) {
			System.out.println(room.getNoOfAdultsAllowed());
			if (noOfPerson >= room.getNoOfAdultsAllowed()) {
				valid += room.getNoOfAdultsAllowed();
			}else {
				return true;
			}
		}
		if(valid >= noOfPerson)
			return true;
		else 
			return false;
	}
	
	
	//function to get customer id corresponding to customer name.
	/*
	 * name		-- name of customer
	 * address		-- address of customer
	 * phone		-- phone of customer
	 * 
	 * returns id of corresponding name
	 * if new customer then adds an entry in customer list and returns id
	 */
	private Long getIdwithName(String name, String address,String phone) {
		String firstName = "";
		String lastName = "";
		
		if (name.indexOf(' ') != -1) {
			firstName = name.substring(0, name.indexOf(' '));
			lastName = name.substring(name.indexOf(' ')+1);
		}else {
			firstName = name;
			lastName ="";
		}
		
		for (int i=0;i<alleUser.size();i++) {
			if (alleUser.get(i).isEmployee()) {
				Mitarbeiter employee = (Mitarbeiter)alleUser.get(i);
				if (employee.getFirstname().equals(firstName)
					&&(employee.getLastname().equals(lastName))) {
					return employee.getId();
				}				
			}else {
				Customer customer = (Customer)alleUser.get(i);
				if (customer.getFirstname().equals(firstName)
						&&(customer.getLastname().equals(lastName))) {
					return customer.getId();
				}
			}	
		}
		Customer newCustomer =new Customer(firstName,lastName,address,phone);
		datenquelleuser.addUser(newCustomer);
		return newCustomer.getId();
		
	}
	
	
	//function to get customer id corresponding to username.
	/*
	 * name		-- username of customer or employee
	 * 
	 * returns id of corresponding username
	 * returns null if no user exists
	 * 
	 */
		private Long getIdwithuserName(String name) {
			
			for (int i=0;i<alleUser.size();i++) {
				if (alleUser.get(i).isEmployee()) {
					Mitarbeiter employee = (Mitarbeiter)alleUser.get(i);
					if (employee.getUsername().equals(name)) {
						return employee.getId();
					}				
				}else {
					Customer customer = (Customer)alleUser.get(i);
					if (customer.getUsername().equals(name)) {
						return customer.getId();
					}
				}	
			}
			
			return null;
			
		}


// function to check if user is employee or not. if user is not employee he cannot cancel or checkout.
// returns false if user not employee
		public boolean chkAuthority(String userName) {
			for (int i=0;i<alleUser.size();i++) {
				if (alleUser.get(i).isEmployee()) {
					Mitarbeiter employee = (Mitarbeiter)alleUser.get(i);
					if (employee.getUsername().equals(userName)) {
						return true;
					}				
				}
			}
			// if user is not an employee
			try {
				throw new CanNotCancel();
			}catch(CanNotCancel ausnahme) {
				Alert alert=new Alert(AlertType.ERROR);
				alert.setContentText("User does not have authority to cancel booking");
				alert.showAndWait();
			}
			return false;
		}


// function to cancel booking of a particular room
		// takes room no  as parameter
	public void cancel(Zimmer_Nummer zimmer_Nummer) {
		for ( ZimmerBooking booking : allBookedRooms) {
			if(   (booking.getZimmerNummer().equals(zimmer_Nummer))   && (booking.getStatus().equals(Zimmer_Status.RESERVED))) {
				datenquellebooking.deleteBooking(booking);
				break;
			}	
		}
	}


	// function to checkout a particulr customer room
			// takes room no  as parameter
	public ZimmerBooking checkout(Zimmer_Nummer zimmer_Nummer) {
		ZimmerBooking deletedBooking = new ZimmerBooking();
		
		for ( ZimmerBooking booking : allBookedRooms) {
			if(   (booking.getZimmerNummer().equals(zimmer_Nummer))   && (booking.getStatus().equals(Zimmer_Status.BESETZT))) {
				deletedBooking = booking;
				datenquellebooking.deleteBooking(booking);
				break;
			}	
		}
		return deletedBooking;
	}



}
