package de.frontend;

import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

import de.backend.ZimmerBooking;
import de.backend.Zimmer_Nummer;
import de.middletier.Auswahl_Criterien;
import de.middletier.Service_All;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class SceneCheckoutController {

	private Stage mainStage;
	private String userName;
	private Service_All myService;
	private ObservableList<Zimmer_Nummer> obListBookedRooms = FXCollections.observableArrayList();
	
	@FXML
	private ComboBox<Zimmer_Nummer> comboRooms;
	
	@FXML
	private Button OKBtn;
	
	
	//The constructor is called first, then any @FXML annotated fields are populated using initialize
    //the constructor does not have access to @FXML fields
    //referring to components defined in the .fxml file, while initialize() does have access to them.
    @FXML
    private void initialize() 
    {
    	comboRooms.setItems(obListBookedRooms);
    	comboRooms.getSelectionModel().clearSelection();

    }
    
    // 	sets mainstage of this from value from calling class
	public void setMainStage(Stage mainStage,String userName ,Service_All myService) {
		this.mainStage=mainStage;
		this.userName= userName;
		this.myService= myService;
		
		List<ZimmerBooking> roomsReserved = myService.auswaehlBooking(Auswahl_Criterien.OCCUPIEDROOMS); 
    	obListBookedRooms.addAll(roomsReserved.stream().map(rooms->rooms.getZimmerNummer()).collect(Collectors.toList()));
		
	}
	
	
	// action handler for click on hyperlink Back
	@FXML
	public void handleOKBtnClick(){
		ZimmerBooking deletedBooking = new ZimmerBooking();
		
		if(comboRooms.getSelectionModel().selectedIndexProperty().equals(null)) {
			deletedBooking = myService.checkout(comboRooms.getValue());
			generate_bill(deletedBooking);
			
			try {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneTwoOptions.fxml"));
				GridPane gitter = (GridPane)loader.load();;
				
				SceneTwoController c1 = loader.getController();
				c1.setMainStage(mainStage,userName,myService);
				
				Scene currentScene = new Scene(gitter,900,900);
				currentScene.getStylesheets().add("file:resourcen/styleSheet2.css");
				currentScene.setCursor(Cursor.CLOSED_HAND);
	
				mainStage.setScene(currentScene);
	    	} catch (IOException e) {
	    			e.printStackTrace();
	    		}	
		}
	}
	
	// function to generate a bill whe customer leaves
		public void generate_bill(ZimmerBooking booking) {
			long numDays;
			double price;
			
			numDays = ChronoUnit.DAYS.between(booking.getStatusActiveOn(), booking.getStatusDeactiveOn());
			price = myService.auswaehlenRooms(Auswahl_Criterien.erstellenSameRoomAsThis(booking.getZimmerNummer())).get(0).getZimmerType().getPreis();
					
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setContentText(" The customer stayed for "+ numDays + " days in the Hotel.  Please collect  " +
			  numDays* price   +"�  from the customer.  Thank you and visit again.");
			alert.showAndWait();
			
		}
		
		// action handler for click on hyperlink Back
		@FXML
		public void handleBackClick(ActionEvent event) {
			try {
	    			FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneTwoOptions.fxml"));
	    			GridPane gitter = (GridPane)loader.load();;
	    			
	    			SceneTwoController c1 = loader.getController();
	    			c1.setMainStage(mainStage,userName,myService);
	    			
	    			Scene currentScene = new Scene(gitter,900,900);
	    			currentScene.getStylesheets().add("file:resourcen/styleSheet2.css");
	    			currentScene.setCursor(Cursor.CLOSED_HAND);
	    			
	    			mainStage.setScene(currentScene);
    		} catch (IOException e) {
    			
    			e.printStackTrace();
    		}
		}
}
