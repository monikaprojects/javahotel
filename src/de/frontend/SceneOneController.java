package de.frontend;

//controller class for SceneOneID.fxml

import java.io.IOException;



import de.backend.ZimmerDAOImplMitMaker;
import de.backend.UserDAOImplMitMaker;
import de.backend.ZimmerBookingImplMitMaker;
import de.middletier.Service_All;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class SceneOneController {
	
	private Service_All myService ;
	private Stage mainStage;
	
	
	@FXML
    private TextField userTextField;
    @FXML
    private PasswordField pwBox;
    @FXML
    private Button OKbtn;
    @FXML
    private Label userEingabe;
    
  //------------------------constructor
    public SceneOneController() {
    	this.myService = new Service_All(new UserDAOImplMitMaker(),new ZimmerDAOImplMitMaker(),new ZimmerBookingImplMitMaker());
	}

// initialise not reqd at present
//    @FXML
//    private void initialize() 
//    {
//    	
//    }
    
    // action handler for sign in button
    @FXML
    public void handleSignInBtnClick(ActionEvent event) {
    	
    	if(userTextField.getText().equals("") || pwBox.getText().equals("")) {
    		userEingabe.setText("Please enter username and password");
    	}else {
	    	if(myService.checkUserFromList(userTextField.getText(), pwBox.getText())){
	    		try {
	    			userEingabe.setText("");
	    			//GridPane gitter = (GridPane)FXMLLoader.load(getClass().getResource("SceneTwoOptions.fxml"));
	    			FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneTwoOptions.fxml"));
	    			GridPane gitter = (GridPane)loader.load();;
	    			
	    			SceneTwoController c1 = loader.getController();
	    			c1.setMainStage(mainStage,userTextField.getText(),myService);
	    			
	    			Scene currentScene = new Scene(gitter,900,900);
	    			currentScene.getStylesheets().add("file:resourcen/styleSheet2.css");
	    			currentScene.setCursor(Cursor.CLOSED_HAND);
	
	    		//	((Stage)OKbtn.getParent().getScene().getWindow()).setScene(firstScene);  or pass stage as parameter
	    			mainStage.setScene(currentScene);
	    		} catch (IOException e) {
	    			
	    			e.printStackTrace();
	    		}
	    	}else {
	    		userEingabe.setText("Please enter correct username and password");
	    	}
    	}
    }
    
 // sets mainstage of this from value from calling class
     public void setMainStage(Stage teststage) {
    	 this.mainStage= teststage;
     }
}

