package de.frontend;

//controller class for SceneBooking.fxml


import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import de.backend.ZimmerDAOImplMitMaker;
import de.backend.ZimmerType;
import de.backend.UserDAOImplMitMaker;
import de.backend.ZimmerBookingImplMitMaker;
import de.middletier.Service_All;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class SceneBookingController {
	
	private Service_All myService ;
	private Stage mainStage;
	private String userName;
	
	
	@FXML
    private ComboBox<String> noOfGuests;
    @FXML
    private ComboBox noOfKids;
    @FXML
    private DatePicker dateOfArrival;
    @FXML
    private DatePicker dateOfLeaving;  
    @FXML
    private ListView<ZimmerType> lstRoomType;
    @FXML
    private ListView lstRoomTypeDesired; 
    @FXML
    private TextField txtName;
    @FXML
    private TextField txtPhone;
    @FXML
    private TextField txtAddress;  
    @FXML
    private Button btnLeft;    
    @FXML
    private Button btnRight;  
    @FXML
    private Button OKbtn;  
    @FXML
    private Label errorLabel;
  
    
    private ObservableList<ZimmerType> obListLeft = FXCollections.observableArrayList(ZimmerType.values());
    private ObservableList<ZimmerType> obListRight = FXCollections.observableArrayList();
    
    
    //------------------------constructor
    public SceneBookingController() {
    	
	}
    
    //The constructor is called first, then any @FXML annotated fields are populated using initialize
    //the constructor does not have access to @FXML fields
    //referring to components defined in the .fxml file, while initialize() does have access to them.
    @FXML
    private void initialize() 
    { 	
    	lstRoomType.setItems(obListLeft);
    	lstRoomTypeDesired.setItems(obListRight);
    	btnLeft.setText("<<");
    	btnRight.setText(">>");
    	errorLabel.setVisible(false);
    	noOfGuests.setValue("");
    	noOfKids.setValue("");
    	dateOfArrival.setValue(LocalDate.now());
    	dateOfLeaving.setValue(LocalDate.now());
    }
	  
	  
  // sets mainstage of this from value from calling class
    public void setMainStage(Stage mainStage,String userName,Service_All myService) {
		this.mainStage=mainStage;
		this.userName= userName;
		this.myService=myService;
		
	}
	
	// action handler for ok button
    @FXML
	public void handleOkbtnClick(){
		List<Boolean>  roomsAllocatedStatus= new ArrayList<Boolean>();
		
		Boolean roomsSufficient=false;;
		
		if ( checkValidData()) {		
		
			List<ZimmerType> rooms = obListRight;
			
			roomsSufficient = myService.buchen(Integer.parseInt(noOfGuests.getValue().toString()),
					Integer.parseInt(noOfKids.getValue().toString()),dateOfArrival.getValue(),dateOfLeaving.getValue(),
					rooms,txtName.getText(),txtPhone.getText() ,txtAddress.getText(),userName,roomsAllocatedStatus);
			
			roomsAllocatedStatus.forEach(room->System.out.println(room));
			if (!roomsSufficient) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setHeaderText( " Available Roomtype are not sufficient for given number of people.");
				alert.showAndWait();
			}
			if( (roomsAllocatedStatus.size() !=0) && chkRoomsAllocatedStatus(roomsAllocatedStatus,rooms)) {			
			
				try {
					FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneTwoOptions.fxml"));
	    			GridPane gitter = (GridPane)loader.load();;
	    			
	    			SceneTwoController c1 = loader.getController();
	    			c1.setMainStage(mainStage,userName,myService);
	    			
	    			Scene currentScene = new Scene(gitter,900,900);
	    			currentScene.getStylesheets().add("file:resourcen/styleSheet2.css");
	    			currentScene.setCursor(Cursor.CLOSED_HAND);
	
	    			mainStage.setScene(currentScene);
		    		} catch (IOException e) {
		    			e.printStackTrace();
		    		}
				}
			} 
	}
	
	//  function to check which rooms are not available on those dates. and display error msg and return false
	// if all rooms are available returns true
	private boolean chkRoomsAllocatedStatus(List<Boolean> roomsAllocatedStatus,List<ZimmerType> rooms) {
		String msg="";
		
		for(int i=0;i<roomsAllocatedStatus.size();i++) {
			if(!roomsAllocatedStatus.get(i)) {
				msg+= rooms.get(i)+ "    ";
			}
		}
		if (!msg.equals("")) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setHeaderText(msg + " These Roomtype are not available for desired dates. Please select other type.");
			alert.showAndWait();
			return false;
		}
		return true;
	}

	// validates data entered by user
	private boolean checkValidData() {
		System.out.println(txtName.getText());
		errorLabel.setText("");
		if (noOfGuests.getValue().equals("")) {
			errorLabel.setText(errorLabel.getText()+ "    Enter number of adults");
			errorLabel.setVisible(true);

			return false;
		}else if (noOfKids.getValue().equals("")) {
			errorLabel.setText(errorLabel.getText()+ "    Enter number of Kids");
			errorLabel.setVisible(true);

			return false;
		}else if (dateOfArrival.equals(null)|| dateOfLeaving.equals(null) ) {
			errorLabel.setText(errorLabel.getText()+ "    Enter  date");
			errorLabel.setVisible(true);

			return false;
		}
		else if(dateOfArrival.getValue().isEqual(dateOfLeaving.getValue()) || dateOfArrival.getValue().isAfter(dateOfLeaving.getValue())) {
			
				errorLabel.setText(errorLabel.getText()+ "    Enter correct date");
				errorLabel.setVisible(true);
	
				return false;
						
		}else if (obListRight.size()==0) {
			errorLabel.setText(errorLabel.getText()+ "    Select Room Type");
			errorLabel.setVisible(true);

			return false;
		}else if (txtName.getText().equals("")) {
			errorLabel.setText(errorLabel.getText()+ "    Enter Name of customer");
			errorLabel.setVisible(true);

			return false;
		}else if (txtPhone.getText().equals("")) {
			errorLabel.setText(errorLabel.getText()+ "    Enter Phone number");
			errorLabel.setVisible(true);

			return false;
		}else if (txtAddress.getText().equals("")) {
			errorLabel.setText(errorLabel.getText()+ "    Enter address");
			errorLabel.setVisible(true);

			return false;
		}
		
		errorLabel.setVisible(false);
		return true;
	}

	// handler for right button
	@FXML
	public void handlebtnRightClick(){		
		obListRight.add(obListLeft.get(lstRoomType.getSelectionModel().getSelectedIndex()));
	}

	// handler for left button
	@FXML
	public void handlebtnLeftClick(){
		obListRight.remove(obListRight.get(lstRoomTypeDesired.getSelectionModel().getSelectedIndex()));
	}

	// action handler for click on hyperlink Back
		@FXML
		public void handleBackClick(ActionEvent event) {
			try {
    			FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneTwoOptions.fxml"));
    			GridPane gitter = (GridPane)loader.load();;
    			
    			SceneTwoController c1 = loader.getController();
    			c1.setMainStage(mainStage,userName,myService);
    			
    			Scene currentScene = new Scene(gitter,900,900);
    			currentScene.getStylesheets().add("file:resourcen/styleSheet2.css");
    			currentScene.setCursor(Cursor.CLOSED_HAND);
    			
    			mainStage.setScene(currentScene);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		}
}
