package de.frontend;
// asks option mitarbeiter or customer
// class to handle communication through console

//--------------------------------------------Not Used

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import de.backend.Zimmer;
import de.middletier.Service_All;

public class KonsoleUI {
	private String user;
	private String pwd;
	private Service_All myService ;	    // object of middletier Service_All
	
	
	/**
	 * KonsoleUI constructor
	 */
	public KonsoleUI(Service_All myService) {
		this.myService = myService;
	}

	
	// function to read user name an password from the scanner
	public void getUserData() {
				
		System.out.println("Enter user name ");
		user= new Scanner(System.in).nextLine();
		System.out.println("Enter password ");
		pwd = new Scanner(System.in).nextLine();
		
	}
	
// read input from scanner what user wants to do.
	private void navigieren() {
		System.out.println("What do you want to do  : 1 for booking , 2 for Cancel,  3 for buying Gutschein , 4 for Exit ");
		int choice=1;		                   // choice is read from console. enter 3 to exit the system
		
		while (choice != 4) {
			try {
				choice = new Scanner(System.in).nextInt();
				switch(choice) {
				case 1: buchen();
						break;
				case 2: Stonieren();
						break;
				case 3: gutscheinKaufen();
						break;
				case 4: break;
				default:
						throw new InvalidEntry();
				}					
			}catch(InvalidEntry ausnahme) {
				System.out.println("Please enter valid digits to continue.......");
			}
		}
	}


	private void gutscheinKaufen() {
			// TODO Auto-generated method stub
			
		}
	
	
	private void Stonieren() {
			// TODO Auto-generated method stub
			
		}
	
	
	private void buchen() {
		int noOfGuests;		// No of people coming to stay
		int noOfKids;		// how many  kids 
		LocalDate dateOfArrival;	// date coming
		LocalDate dateOfLeaving;    // date leaving
		List<Zimmer> freeRoomList = new ArrayList<Zimmer>();
		
		System.out.println("How many guests are coming:  ");
		noOfGuests = new Scanner(System.in).nextInt();
		System.out.println("How many kids are coming:  ");
		noOfKids = new Scanner(System.in).nextInt();
		
//		System.out.println("Date of Arrival:  ");
//		dateOfArrival = LocalDate.parse(new Scanner(System.in).nextLine());
//		System.out.println("Date of leaving:  ");
//		dateOfLeaving = LocalDate.parse(new Scanner(System.in).nextLine());
		//Predicate<Zimmer> freeRooms =  room -> room.getStatus().equals(Zimmer_Status.FREI);
		//freeRoomList = myService.auswaehlRooms(freeRooms);
		int extraBed = noOfKids%2+1;
		//freeRoomList.forEach (room -> System.out.println(room.getZimmerNummer().getDigit()) );
		freeRoomList.forEach (room -> System.out.println(room.toString()) );
		}



	// function to launch the system.
	public void launch() {
		System.out.println("********************************Welcome to Hotel Rossa in Berlin*********************************");
		getUserData();
		if (myService.checkUserFromList(user,pwd)) {
			navigieren();
		}
		
		System.out.println("Please visit again.");
	}

}
