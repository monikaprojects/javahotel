package de.frontend;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;


public class GraphicsUI extends Application{
	private Stage mainStage;	
	
	
	@Override
	public void start(Stage primaryStage)  {
		mainStage= primaryStage;
		primaryStage.setTitle("Majestic!");
	//	primaryStage.setFullScreen(true);
		
		createSceneOne();
		primaryStage.show();
		
	}


	// loads SceneOneID.fxml and displays in main stage
	private void createSceneOne() {
		try {
			//gitter = (GridPane)FXMLLoader.load(getClass().getResource("sceneone.fxml"));  or
			FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneOneID.fxml"));
			GridPane gitter = (GridPane)loader.load();
			
			SceneOneController c1 = loader.getController();
			c1.setMainStage(mainStage);

			Scene currentScene = new Scene(gitter,900,900);
			//currentScene.getStylesheets().add("https://fonts.googleapis.com/css2?family=Rock+Salt&display=swap");
			//currentScene.getStylesheets().add("file:src/de/frontend/styleSheet1.css");
			currentScene.getStylesheets().add("file:resourcen/styleSheet1.css");
			currentScene.setCursor(Cursor.OPEN_HAND);
			
			mainStage.setScene(currentScene);
		} catch (IOException e) {	
			e.printStackTrace();
		}
	}

}
