package de.frontend;


// controller class for SceneTwoOptions.fxml

import java.io.IOException;

import de.middletier.Service_All;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class SceneTwoController {

	private Stage mainStage;
	private String userName;
	private Service_All myService;
	
	// sets mainstage of this from value from calling class
	public void setMainStage(Stage mainStage,String userName ,Service_All myService) {
		this.mainStage=mainStage;
		this.userName= userName;
		this.myService= myService;
		
	}
	
	// action handler for click on hyperlink booking
	@FXML
    public void handleBookingClick(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneBooking.fxml"));
			GridPane gitter = (GridPane)loader.load();;
			
			SceneBookingController c1 = loader.getController();
			c1.setMainStage(mainStage,userName,myService);

			Scene currentScene = new Scene(gitter,900,900);
			currentScene.getStylesheets().add("file:resourcen/styleSheet3.css");
			currentScene.setCursor(Cursor.DEFAULT);
			mainStage.setScene(currentScene);
		} catch (IOException e) {	
			e.printStackTrace();
		}
	}
	
	// action handler for click on hyperlink Cancel
	@FXML
	public void handleCancelClick(ActionEvent event) {
		if (myService.chkAuthority(userName)) {
			try {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneCancel.fxml"));
				GridPane gitter = (GridPane)loader.load();;
				
				SceneCancelController c1 = loader.getController();
				c1.setMainStage(mainStage,userName,myService);

				Scene currentScene = new Scene(gitter,900,900);
				currentScene.getStylesheets().add("file:resourcen/styleSheet4.css");
				currentScene.setCursor(Cursor.DEFAULT);
				mainStage.setScene(currentScene);
			} catch (IOException e) {	
				e.printStackTrace();
			}
		}
			
	}
	
	// action handler for click on hyperlink Gutschein
	@FXML
	public void handleGutscheinClick(ActionEvent event) {
		
	}
	
	// action handler for click on hyperlink Exit
	@FXML
	public void handleExitClick(ActionEvent event) {
		System.exit(1);
	}
	
	// action handler for click on hyperlink Checkout
	@FXML
	public void handleCheckoutClick(ActionEvent event) {
		
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneCheckout.fxml"));
			GridPane gitter = (GridPane)loader.load();;
			
			SceneCheckoutController c1 = loader.getController();
			c1.setMainStage(mainStage,userName,myService);

			Scene currentScene = new Scene(gitter,900,900);
			currentScene.getStylesheets().add("file:resourcen/styleSheet5.css");
			currentScene.setCursor(Cursor.DEFAULT);
			mainStage.setScene(currentScene);
		} catch (IOException e) {	
			e.printStackTrace();
		}
	}
	
	// action handler for click on hyperlink Back
	@FXML
	public void handleBackClick(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("SceneOneID.fxml"));
			GridPane gitter = (GridPane)loader.load();;
			
			SceneOneController c1 = loader.getController();
			c1.setMainStage(mainStage);

			Scene currentScene = new Scene(gitter,900,900);
			currentScene.getStylesheets().add("file:resourcen/styleSheet1.css");
			currentScene.setCursor(Cursor.OPEN_HAND);
			mainStage.setScene(currentScene);
		} catch (IOException e) {	
			e.printStackTrace();
		}
	}
	
}

	