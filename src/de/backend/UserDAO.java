package de.backend;

import java.util.List;

/**
 * 
 * Part of the DAO design pattern:
 * Generalizes the access from the data storage layer
 * Middletier only communicates with the interface,
 * and has no knowledge of the implementation
 * 
 */


public interface UserDAO {
	List<Person> getAllUsers(); 		//SELECT
	void addUser(Person person);		//INSERT INTO
	void deleteUser(Person person);		//DELETE
	void updateUser(Person person);		//UPDATE
}
