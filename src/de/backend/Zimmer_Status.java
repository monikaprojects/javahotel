package de.backend;

//Enum for Room status. Room can be in any of these states

public enum Zimmer_Status {
	FREI,				// Room is free
	BESETZT,			// Room is occupied
	AUFRAUMEN,			// Room is set for cleaning as a customer has just left
	RESERVED,			// Room is reserved for particular dates
	NICHTGEEIGNET;		// Room is under construction or renovation
}
