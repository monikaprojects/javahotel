package de.backend;

import java.util.List;

/**
 * 
 * Part of the DAO design pattern:
 * Generalizes the access from the data storage layer
 * Middletier only communicates with the interface,
 * and has no knowledge of the implementation
 */


public interface ZimmerDAO {
	List<Zimmer> getAllRooms(); 		//SELECT
	void addRoom(Zimmer room);			//INSERT INTO
	void deleteRoom(Zimmer room);		//DELETE
	void updateRoom(Zimmer room);		//UPDATE
}
