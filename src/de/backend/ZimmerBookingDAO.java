/**
 * 
 */
package de.backend;

import java.util.List;

/**
 * Part of the DAO design pattern:
 * Generalizes the access from the data storage layer
 * Middletier only communicates with the interface,
 * and has no knowledge of the implementation
 *
 */


public interface ZimmerBookingDAO {
	List<ZimmerBooking> getAllBooking();			 //SELECT
	void addBooking(ZimmerBooking booking);			//INSERT INTO
	void deleteBooking(ZimmerBooking booking);		//DELETE
	void updateBooking(ZimmerBooking booking);		//UPDATE
	
}
