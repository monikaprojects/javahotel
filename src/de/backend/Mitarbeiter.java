package de.backend;

//derives from class Person.
public class Mitarbeiter extends Person{
	
	// one constant object of type mitarbeiter 
	public static final Mitarbeiter OWNER = new Mitarbeiter("Owner", "Owner", 30, "Owner", "Owner", "Owner", "Owner");

	private String position;     	// designation of employee
	private double salary;			// Gehalt
	private String username;		// username to login in this system
	private String 	password;		//password of user to login in this system

	
	 // constructor of class Mitarbeiter which extends person. 
	public Mitarbeiter(String firstname, String lastname, int alter, String anschrift, String phoneNo,
			String maritalStatus, String emailId) {
		super(firstname, lastname, alter, anschrift, phoneNo, maritalStatus, emailId);
		position = "";
		salary=0.0;
		username =firstname;
		password=firstname;
		isEmployee = true;			// True set for Mitarbeiter
	}
	
	//---------------------------------------getters and setters
	/**
	 * @return the position
	 */
	public String getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(String position) {
		this.position = position;
	}

	/**
	 * @return the salary
	 */
	public double getSalary() {
		return salary;
	}


	/**
	 * @param salary the salary to set
	 */
	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

}
