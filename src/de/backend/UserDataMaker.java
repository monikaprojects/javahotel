package de.backend;

/*
 * We can generate data from this or from database
 * Simulates the Datenbank
 */
import java.util.ArrayList;
import java.util.List;


public class UserDataMaker  {

	private List<Person> userList= new ArrayList<Person>();
	
	// constructor which generates objects of type user
	public UserDataMaker(){
		Customer newCust = new Customer("Sherlock","Holms",40,"Berlin","0151242324232","Unmarried","holmes@gmail.com");
		newCust.setRegular_customer(false);
		userList.add(newCust);
		
		newCust = new Customer("Aryan","Neuhaus",30,"Mannheim","0151242234232","Unmarried","Neuhaus@gmail.com");
		newCust.setRegular_customer(true);
		userList.add(newCust);
		
		newCust = new Customer("Mili","Gupta",50,"L�beck","01512567524232","married","Gupta@gmail.com");
		newCust.setRegular_customer(false);
		userList.add(newCust);
		
		newCust = new Customer("Anzelina","Koch",43,"Berlin","0151456324232","Unmarried","Koch@gmail.com");
		newCust.setRegular_customer(true);
		userList.add(newCust);
		
		newCust = new Customer("Oliver","Zuern",24,"Bayern","0151242324672","married","holmes@gmail.com");
		newCust.setRegular_customer(false);
		userList.add(newCust);
		
		newCust = new Customer("s","s",24,"Bayern","0151242324672","married","holmes@gmail.com");
		newCust.setRegular_customer(false);
		userList.add(newCust);

		
		
		//  add employees
		Mitarbeiter newMitarbeiter = new Mitarbeiter("Alex","Cicilia",30,"Braddenburg","0151245286232","Unmarried","Cicilia@gmail.com");
		newMitarbeiter.setPosition("Owner");
		newMitarbeiter.setSalary(200000.00);
		userList.add(newMitarbeiter);
		
		newMitarbeiter = new Mitarbeiter("Christin","Chow",43,"Frankfurt","015567324232","Unmarried","Chow@gmail.com");
		newMitarbeiter.setPosition("Manager");
		newMitarbeiter.setSalary(120000.00);
		userList.add(newMitarbeiter);
		
		newMitarbeiter = new Mitarbeiter("Harry","Holmes",47,"Hessen","0151434324232","Unmarried","H.Holmes@gmail.com");
		newMitarbeiter.setPosition("Receptionist");
		newMitarbeiter.setSalary(60000.00);
		userList.add(newMitarbeiter);
		
		newMitarbeiter = new Mitarbeiter("Rosy","Bl�tter",30,"Heidelberg","01512429456232","Unmarried","Bl�tter@gmail.com");
		newMitarbeiter.setPosition("Receptionist");
		newMitarbeiter.setSalary(65000.00);
		userList.add(newMitarbeiter);
		
		newMitarbeiter = new Mitarbeiter("a","Bl�tter",30,"Heidelberg","01512429456232","Unmarried","Bl�tter@gmail.com");
		newMitarbeiter.setPosition("Receptionist");
		newMitarbeiter.setSalary(65000.00);
		userList.add(newMitarbeiter);
	}

	
	//--------------------------------------------methods
	public List<Person> getListOfUsers(){
		return userList;
	}
	
}
