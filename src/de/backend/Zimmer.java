package de.backend;

//Class to generate objects of type Zimmer which has room number,room type etc. as attributes

public class Zimmer {
	
	private Zimmer_Nummer zimmerNummer;  			// room No. of type Enum
	private ZimmerType zimmerType;  				// room type of type Enum -- EinzelZimmer or DoppelZimmer
	private int floor;								// on which floor is this room
	
	
	// constructor to generate a Zimmer Object	
	/**
	 * @param zimmerNummer
	 * @param zimmerType
	 * @param status
	 * @param facilities
	 */
	public Zimmer(Zimmer_Nummer zimmerNummer, ZimmerType zimmerType, int floor) {
		this.zimmerNummer = zimmerNummer;
		this.zimmerType = zimmerType;
		this.floor=floor;	
	}
	//default constructor
	public Zimmer() {
	}

	

	//---------------------------------------getters and setters
	/**
	 * @return the zimmerNummer
	 */
	public Zimmer_Nummer getZimmerNummer() {
		return zimmerNummer;
	}

	/**
	 * @return the zimmerType
	 */
	public ZimmerType getZimmerType() {
		return zimmerType;
	}

	/**
	 * @return the floor
	 */
	public int getFloor() {
		return floor;
	}

	//---------------------------------------tostring override
	@Override
	public String toString() {
		return zimmerNummer.getDigit()+ "   "  +zimmerType ;
	}


	

}
