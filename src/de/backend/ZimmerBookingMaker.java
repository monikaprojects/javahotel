package de.backend;

/*
 * We can generate data from this or from database
 * Simulates the Datenbank
 */

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ZimmerBookingMaker {
	
	private List<ZimmerBooking> roomList= new ArrayList<ZimmerBooking>();
	
	// constructor which generates objects of type ZimmerBooking
	public ZimmerBookingMaker() {
		ZimmerBooking newRoom = new ZimmerBooking(Zimmer_Nummer.EINHUNDERTEINS, Zimmer_Status.FREI,LocalDate.now(),LocalDate.now(),Mitarbeiter.OWNER.getId(),Mitarbeiter.OWNER.getId());
		roomList.add(newRoom);
		newRoom = new ZimmerBooking(Zimmer_Nummer.EINHUNDERTZWEI, Zimmer_Status.BESETZT,LocalDate.of(2021, 03, 01),LocalDate.of(2021, 05, 01),Mitarbeiter.OWNER.getId(),Mitarbeiter.OWNER.getId());
		roomList.add(newRoom);
		newRoom = new ZimmerBooking(Zimmer_Nummer.EINHUNDERTDREI, Zimmer_Status.AUFRAUMEN,LocalDate.of(2021, 04, 01),LocalDate.of(2021, 04, 01),Mitarbeiter.OWNER.getId(),Mitarbeiter.OWNER.getId());
		roomList.add(newRoom);
		newRoom = new ZimmerBooking(Zimmer_Nummer.EINHUNDERTVIER,  Zimmer_Status.RESERVED,LocalDate.of(2021, 04, 10),LocalDate.of(2021, 04, 12),Mitarbeiter.OWNER.getId(),Mitarbeiter.OWNER.getId());
		roomList.add(newRoom);
		
		newRoom = new ZimmerBooking(Zimmer_Nummer.ZWEIHUNDERTEINS, Zimmer_Status.BESETZT,LocalDate.of(2021, 03, 01),LocalDate.of(2021, 05, 01),Mitarbeiter.OWNER.getId(),Mitarbeiter.OWNER.getId());
		roomList.add(newRoom);
		newRoom = new ZimmerBooking(Zimmer_Nummer.ZWEIHUNDERTZWEI, Zimmer_Status.FREI,LocalDate.now(),LocalDate.now());
		roomList.add(newRoom);
		newRoom = new ZimmerBooking(Zimmer_Nummer.ZWEIHUNDERTDREI, Zimmer_Status.NICHTGEEIGNET,LocalDate.of(2021, 03, 01),LocalDate.of(2021, 05, 01),Mitarbeiter.OWNER.getId(),Mitarbeiter.OWNER.getId());
		roomList.add(newRoom);
		newRoom = new ZimmerBooking(Zimmer_Nummer.ZWEIHUNDERTVIER, Zimmer_Status.AUFRAUMEN,LocalDate.of(2021, 03, 01),LocalDate.of(2021, 05, 01),Mitarbeiter.OWNER.getId(),Mitarbeiter.OWNER.getId());
		roomList.add(newRoom);
		
		newRoom = new ZimmerBooking(Zimmer_Nummer.DREIHUNDERTEINS,  Zimmer_Status.AUFRAUMEN,LocalDate.of(2021, 03, 01),LocalDate.of(2021, 05, 01),Mitarbeiter.OWNER.getId(),Mitarbeiter.OWNER.getId());
		roomList.add(newRoom);
		newRoom = new ZimmerBooking(Zimmer_Nummer.DREIHUNDERTZWEI,  Zimmer_Status.FREI,LocalDate.now(),LocalDate.now(),Mitarbeiter.OWNER.getId(),Mitarbeiter.OWNER.getId());
		roomList.add(newRoom);
		newRoom = new ZimmerBooking(Zimmer_Nummer.DREIHUNDERTDREI, Zimmer_Status.FREI, LocalDate.now(),LocalDate.now(),Mitarbeiter.OWNER.getId(),Mitarbeiter.OWNER.getId());
		roomList.add(newRoom);
		newRoom = new ZimmerBooking(Zimmer_Nummer.DREIHUNDERTVIER,  Zimmer_Status.NICHTGEEIGNET,LocalDate.of(2021, 03, 01),LocalDate.of(2021, 05, 01),Mitarbeiter.OWNER.getId(),Mitarbeiter.OWNER.getId());
		roomList.add(newRoom);
		
		newRoom = new ZimmerBooking(Zimmer_Nummer.VIERHUNDERTEINS,  Zimmer_Status.FREI,LocalDate.now(),LocalDate.now(),Mitarbeiter.OWNER.getId(),Mitarbeiter.OWNER.getId());
		roomList.add(newRoom);
		newRoom = new ZimmerBooking(Zimmer_Nummer.VIERHUNDERTZWEI,Zimmer_Status.RESERVED,LocalDate.of(2021, 03, 01),LocalDate.of(2021, 05, 01),Mitarbeiter.OWNER.getId(),Mitarbeiter.OWNER.getId());
		roomList.add(newRoom);
		newRoom = new ZimmerBooking(Zimmer_Nummer.VIERHUNDERTDREI, Zimmer_Status.AUFRAUMEN, LocalDate.of(2021, 03, 01),LocalDate.of(2021, 05, 01),Mitarbeiter.OWNER.getId(),Mitarbeiter.OWNER.getId());
		roomList.add(newRoom);
		newRoom = new ZimmerBooking(Zimmer_Nummer.VIERHUNDERTVIER,  Zimmer_Status.FREI, LocalDate.now(),LocalDate.now(),Mitarbeiter.OWNER.getId(),Mitarbeiter.OWNER.getId());
		roomList.add(newRoom);
	}
	

	
	//--------------------------------------------methods
	public List<ZimmerBooking> getListOfRooms() {
		return roomList;
	}
	
}
