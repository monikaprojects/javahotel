package de.backend;

// derives from class Person.

public class Customer extends Person{

	private boolean regular_customer ;			// if regular customer then set it to true else false
	private String password;					//password of user to login in this system
	private String username;					// username to login in this system
	
	
	// constructor to generate a Customer Object only with name ,address and phone no. Reqd if new customer has come to stay
	public Customer(String firstname, String lastname, String anschrift, String phoneNo) {
		super(firstname, lastname,  anschrift, phoneNo );
		regular_customer= false;
		username =firstname;
		password=firstname;
		isEmployee =false;
	}
	
	 // constructor of class customer which extends person. 
	public Customer(String firstname, String lastname, int alter, String anschrift, String phoneNo,
			String maritalStatus, String emailId) {
		super(firstname, lastname, alter, anschrift, phoneNo, maritalStatus, emailId);
		regular_customer= false;
		username =firstname;
		password=firstname;
		isEmployee =false;
	}
	
	//---------------------------------------getters and setters
	/**
	 * @return the regular_customer
	 */
	public boolean isRegular_customer() {
		return regular_customer;
	}

	/**
	 * @param regular_customer the regular_customer to set
	 */
	public void setRegular_customer(boolean regular_customer) {
		this.regular_customer = regular_customer;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

}
