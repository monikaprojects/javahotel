package de.backend;

/*
 * We can generate data from this or from database
 * Simulates the Datenbank
 */
import java.util.ArrayList;
import java.util.List;

public class ZimmerDataMaker {
	private List<Zimmer> roomList= new ArrayList<Zimmer>();

	// constructor which generates objects of type Zimmer
	public ZimmerDataMaker() {
		Zimmer newRoom = new Zimmer(Zimmer_Nummer.EINHUNDERTEINS, ZimmerType.EINBETTZIMMER_COMFORT,1);
		roomList.add(newRoom);
		newRoom = new Zimmer(Zimmer_Nummer.EINHUNDERTZWEI, ZimmerType.DOPPELZIMMER, 1);
		roomList.add(newRoom);
		newRoom = new Zimmer(Zimmer_Nummer.EINHUNDERTDREI, ZimmerType.EINZELZIMMERMITKINDERBETT, 1);
		roomList.add(newRoom);
		newRoom = new Zimmer(Zimmer_Nummer.EINHUNDERTVIER, ZimmerType.DOPPLZIMMERMITKINDERBETT, 1);
		roomList.add(newRoom);
		
		newRoom = new Zimmer(Zimmer_Nummer.ZWEIHUNDERTEINS, ZimmerType.DOPPELZIMMER, 2);
		roomList.add(newRoom);
		newRoom = new Zimmer(Zimmer_Nummer.ZWEIHUNDERTZWEI, ZimmerType.EINZELZIMMERMITKINDERBETT, 2);
		roomList.add(newRoom);
		newRoom = new Zimmer(Zimmer_Nummer.ZWEIHUNDERTDREI, ZimmerType.EINBETTZIMMER_COMFORT, 2);
		roomList.add(newRoom);
		newRoom = new Zimmer(Zimmer_Nummer.ZWEIHUNDERTVIER, ZimmerType.DOPPLZIMMERMITKINDERBETT, 2);
		roomList.add(newRoom);
		
		newRoom = new Zimmer(Zimmer_Nummer.DREIHUNDERTEINS, ZimmerType.SUITE, 3);
		roomList.add(newRoom);
		newRoom = new Zimmer(Zimmer_Nummer.DREIHUNDERTZWEI, ZimmerType.DOPPELZIMMER_MIT_RHEINVIEW,3);
		roomList.add(newRoom);
		newRoom = new Zimmer(Zimmer_Nummer.DREIHUNDERTDREI, ZimmerType.DOPPELZIMMER, 3);
		roomList.add(newRoom);
		newRoom = new Zimmer(Zimmer_Nummer.DREIHUNDERTVIER, ZimmerType.EINBETTZIMMER_COMFORT, 3);
		roomList.add(newRoom);
		
		newRoom = new Zimmer(Zimmer_Nummer.VIERHUNDERTEINS, ZimmerType.EINZELZIMMERMITKINDERBETT,4);
		roomList.add(newRoom);
		newRoom = new Zimmer(Zimmer_Nummer.VIERHUNDERTZWEI, ZimmerType.SUITE, 4);
		roomList.add(newRoom);
		newRoom = new Zimmer(Zimmer_Nummer.VIERHUNDERTDREI, ZimmerType.EINBETTZIMMER_COMFORT, 4);
		roomList.add(newRoom);
		newRoom = new Zimmer(Zimmer_Nummer.VIERHUNDERTVIER, ZimmerType.DOPPELZIMMER_MIT_RHEINVIEW,4);
		roomList.add(newRoom);
	}
	

	//--------------------------------------------methods
	public List<Zimmer> getListOfRooms() {
		return roomList;
	}

}
