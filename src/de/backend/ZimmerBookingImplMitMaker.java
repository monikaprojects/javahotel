package de.backend;

import java.util.List;
/**
 * 
 * Implements the DAO-Interface.
 * Here the data is generated in a list of constants and used during Entwicklungs-Phase
 */
public class ZimmerBookingImplMitMaker implements ZimmerBookingDAO{

	private ZimmerBookingMaker  datenbankroom;
	private List<ZimmerBooking> bookingList;
	
	// constructor 
		public ZimmerBookingImplMitMaker() {
			datenbankroom= new ZimmerBookingMaker();
		}
		
		
		//----------------------------methods
		
		// returns the list of booking generated by Maker
	@Override
	public List<ZimmerBooking> getAllBooking() {
		bookingList = datenbankroom.getListOfRooms();
		return bookingList;
	}

	// add a new booking to the list
	@Override
	public void addBooking(ZimmerBooking booking) {
		bookingList.add(booking);
		
	}

	// removes a booking from list
	@Override
	public void deleteBooking(ZimmerBooking booking) {
		bookingList.remove(booking);
		
	}

	// does not have any use in this case 
	@Override
	public void updateBooking(ZimmerBooking booking) {
		// TODO Auto-generated method stub
		
	}

}
