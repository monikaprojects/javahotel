package de.backend;

//Enum for Room type. Room can be  any of these types:each type has an associated price,
//facilities,number of people allowed in that room

public enum ZimmerType {
	// Possible Room types in this hotel
	EINBETTZIMMER_COMFORT(89.0,"Single room 1.60 cm mattress, modern shower room mostly with a view of the lake",1),
					// single room
	EINZELZIMMERMITKINDERBETT(99.0,"Single room 1.60 cm mattress, plus extra bed for kids",1),
					// single room with kids bed
	DOPPELZIMMER(109.0,"Double room 1.80 cm mattress, modern shower room with a view of the fountain",2),
					// double room 
	DOPPELZIMMER_MIT_RHEINVIEW(139.0,"Double room on  upper floor 1.80cm mattress, modern shower room mostly with a view of the lake",2),
					// double room with river view
	DOPPLZIMMERMITKINDERBETT(140.0,"Double room on  upper floor 1.80cm mattress, modern shower room mostly",2),
					// double room with kids bed
	SUITE(180.0,"Double room on  upper floor 1.80cm mattress, bathtub, sitting area, mostly with a view of the lake",4);
						// suite 
	
	private double preis;
	private String facilities;
	private int noOfAdultsAllowed;
	
	// constructor 
	ZimmerType(double preis,String facilities,int noOfAdultsAllowed){
		this.preis= preis;
		this.facilities=facilities;
		this.noOfAdultsAllowed=noOfAdultsAllowed;
	}
	
	
	// methods

	/**
	 * @return the price
	 */
	public double getPreis() {
		return preis;
	}

	/**
	 * @return the facilities
	 */
	public String getFacilities() {
		return facilities;
	}

	/**
	 * @return the noOfAdultsAllowed
	 */
	public int getNoOfAdultsAllowed() {
		return noOfAdultsAllowed;
	}
	
	
}
