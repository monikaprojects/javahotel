package de.backend;

// Class to generate objects of type Person which has name,address etc. as attributes

public class Person {
	protected Long id;					// primary key for person cannot be null
	protected String firstname;  		// FIRST NAME OF PERSON
	protected String lastname;			// LAST NAME OF PERSON
	protected int alter;				// AGE  OF PERSON
	protected String anschrift;			//ADDRESS OF PERSON
	protected String phoneNo;			// PHONE NO OF PERSON
	protected String maritalStatus;		// MARRIED OR UNMARRIED OR DIVORCED 
	protected String emailId; 			// EMAIL ID  OF PERSON
	protected boolean isEmployee;  		// true of employees of this institute
	private static  Long max=(long) 1;	// gives the last generated  id of Person
	
	
	// constructor to generate a Person Object only with name ,address and phone no. Reqd if new customer has come to stay
	/**
	 * @param firstname
	 * @param lastname
	 * @param anschrift
	 * @param phoneNo
	 */
	public Person(String firstname, String lastname, String anschrift, String phoneNo) {
		this.id=max;
		this.firstname = firstname;
		this.lastname = lastname;
		this.anschrift = anschrift;
		this.phoneNo = phoneNo;
		this.isEmployee = false;
		max+=1;
	}
	
	/**
	 * @param firstname
	 * @param lastname
	 * @param alter
	 * @param anschrift
	 * @param phoneNo
	 * @param maritalStatus
	 * @param emailId
	 */
	public Person(String firstname, String lastname, int alter, String anschrift, String phoneNo, String maritalStatus,
			String emailId) {
		this.id=max;
		this.firstname = firstname;
		this.lastname = lastname;
		this.alter = alter;
		this.anschrift = anschrift;
		this.phoneNo = phoneNo;
		this.maritalStatus = maritalStatus;
		this.emailId = emailId;
		this.isEmployee = false;
		max+=1;
	}
	
	
	//---------------------------------------getters and setters
	/**
	 * @return the anschrift
	 */
	public String getAnschrift() {
		return anschrift;
	}
	/**
	 * @param anschrift the anschrift to set
	 */
	public void setAnschrift(String anschrift) {
		this.anschrift = anschrift;
	}
	/**
	 * @return the phoneNo
	 */
	public String getPhoneNo() {
		return phoneNo;
	}
	/**
	 * @param phoneNo the phoneNo to set
	 */
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	/**
	 * @return the maritalStatus
	 */
	public String getMaritalStatus() {
		return maritalStatus;
	}
	/**
	 * @param maritalStatus the maritalStatus to set
	 */
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}
	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}
	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}
	/**
	 * @return the alter
	 */
	public int getAlter() {
		return alter;
	}
	
	/**
	 * @return the id  primary key
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the isEmployee
	 */
	public boolean isEmployee() {
		return isEmployee;
	}
	/**
	 * @param isEmployee the isEmployee to set
	 */
	public void setEmployee(boolean isEmployee) {
		this.isEmployee = isEmployee;
	}
	
	
	//---------------------------------------tostring override
	@Override
	public String toString() {
		return "Person [firstname=" + firstname + ", lastname=" + lastname + ", alter=" + alter + ", anschrift="
				+ anschrift + ", phoneNo=" + phoneNo + ", maritalStatus=" + maritalStatus + ", emailId=" + emailId
				+ "]";
	}
	
	//---------------------------------------hashcode and equal override
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((phoneNo == null) ? 0 : phoneNo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (phoneNo == null) {
			if (other.phoneNo != null)
				return false;
		} else if (!phoneNo.equals(other.phoneNo))
			return false;
		return true;
	}
	
	
	
	
}



