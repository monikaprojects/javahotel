package de.backend;

//Class to generate objects of type ZimmerBooking which has all details of rooms and their status

import java.time.LocalDate;

public class ZimmerBooking {
	private Zimmer_Nummer zimmerNummer;  				// room No.
	private Zimmer_Status status;    					// free or booked or cleaning or renovation
	private LocalDate statusActiveOn;  					// from what date is this status active
	private LocalDate statusDeactiveOn;  				// till what date is this status active
	private Long booked_for;   							// customer id
	private Long booked_by;								// booked by whom customer or mitarbeiter. who did this booking
	
	
	
	
	// constructor to generate a ZimmerBooking Object
	// default constructor
	public ZimmerBooking() {
	}
	
	/**
	 * @param zimmerNummer
	 * @param status
	 * @param statusActiveOn
	 * @param statusDeactiveOn
	 */
	public ZimmerBooking(Zimmer_Nummer zimmerNummer, Zimmer_Status status, LocalDate statusActiveOn,
			LocalDate statusDeactiveOn) {
		this.zimmerNummer = zimmerNummer;
		this.status = status;
		this.statusActiveOn = statusActiveOn;
		this.statusDeactiveOn = statusDeactiveOn;
	}

	public ZimmerBooking(Zimmer_Nummer zimmerNummer, Zimmer_Status status, LocalDate statusActiveOn,
			LocalDate statusDeactiveOn,Long booked_for,Long booked_by) {
		this.zimmerNummer = zimmerNummer;
		this.status = status;
		this.statusActiveOn = statusActiveOn;
		this.statusDeactiveOn = statusDeactiveOn;
		this.booked_for =booked_for;
		this.booked_by= booked_by;
	}
	
	
	//---------------------------------------getters and setters
	/**
	 * @return the zimmerNummer
	 */
	public Zimmer_Nummer getZimmerNummer() {
		return zimmerNummer;
	}


	/**
	 * @param zimmerNummer the zimmerNummer to set
	 */
	public void setZimmerNummer(Zimmer_Nummer zimmerNummer) {
		this.zimmerNummer = zimmerNummer;
	}

	/**
	 * @return the status
	 */
	public Zimmer_Status getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Zimmer_Status status) {
		this.status = status;
	}

	/**
	 * @return the statusActiveOn
	 */
	public LocalDate getStatusActiveOn() {
		return statusActiveOn;
	}

	/**
	 * @param statusActiveOn the statusActiveOn to set
	 */
	public void setStatusActiveOn(LocalDate statusActiveOn) {
		this.statusActiveOn = statusActiveOn;
	}

	/**
	 * @return the statusDeactiveOn
	 */
	public LocalDate getStatusDeactiveOn() {
		return statusDeactiveOn;
	}

	/**
	 * @param statusDeactiveOn the statusDeactiveOn to set
	 */
	public void setStatusDeactiveOn(LocalDate statusDeactiveOn) {
		this.statusDeactiveOn = statusDeactiveOn;
	}

	/**
	 * @return the booked_for
	 */
	public Long getBooked_for() {
		return booked_for;
	}

	/**
	 * @param booked_for the booked_for to set
	 */
	public void setBooked_for(Long booked_for) {
		this.booked_for = booked_for;
	}

	/**
	 * @return the booked_by
	 */
	public Long getBooked_by() {
		return booked_by;
	}

	/**
	 * @param booked_by the booked_by to set
	 */
	public void setBooked_by(Long booked_by) {
		this.booked_by = booked_by;
	}

	//---------------------------------------tostring override
	
	@Override
	public String toString() {
		return "ZimmerBooking [zimmerNummer=" + zimmerNummer + ", status=" + status + ", statusActiveOn="
				+ statusActiveOn + ", statusDeactiveOn=" + statusDeactiveOn + ", booked_for=" + booked_for
				+ ", booked_by=" + booked_by + "]";
	}
	
	
	
}
