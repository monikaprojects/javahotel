package de.backend;

// Enum for Room number. Has a  function getDigit() which returns room number in digits


public enum Zimmer_Nummer {
	// Possible Room numbers in this hotel
	EINHUNDERTEINS(101),EINHUNDERTZWEI(102),EINHUNDERTDREI(103),EINHUNDERTVIER(104),
	ZWEIHUNDERTEINS(201),ZWEIHUNDERTZWEI(202),ZWEIHUNDERTDREI(203),ZWEIHUNDERTVIER(204),
	DREIHUNDERTEINS(301),DREIHUNDERTZWEI(302),DREIHUNDERTDREI(303),DREIHUNDERTVIER(304),
	VIERHUNDERTEINS(401),VIERHUNDERTZWEI(402),VIERHUNDERTDREI(403),VIERHUNDERTVIER(404);
	
	private int inDigit ;				// stores room number in digits
	
	// constructor 
	Zimmer_Nummer(int i) {
		inDigit = i;
	}
	
	// method
	public int getDigit() {
		return inDigit;
	}
	

}
